## PyTest Examples
Simple examples on how to structure tests with PyTest. https://pytest.org

(These are just my personal notes from Richard Wells' course Unit Testing and TDD in Python)

### Setup
1. `git clone` this project.
2. `python3 -m venv .venv`
3. `source .venv/bin/activate`
4. `pip install pytest`
5. `pytest -v test_file.py`

### Specifying what tests should run
- By default PyTest automatically runs tests for propertly named modules from current working directory
- Commands for controlling which test are discovered and executed:
    - `moduleName` - only run tests of that module
    - `DirectoryName/` - only runs tests in specified dir
    - `-k "expression"` - matches tests with string expression. can be module/class/function names
    - `-m "expression"` - matches test with 'pytest.mark' decorator that matches expression

### Other useful args
- `-v` - verbose mode
- `-q` - quiet mode
- `-s` - show print statements in console
- `--ignore` - ignore specified path with discovering tests
- `--maxfail` - stop after specified number of failures