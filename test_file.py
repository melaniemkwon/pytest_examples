'''
Pytest test discovery

NAMING CONVENTIONS:
    - test functions should start with 'test'
    - test classes should start with 'Test' and have no __init__ method
    - filenames should be either start/end with 'test'
            test_example.py or example_test.py
'''

def test_me():
    assert True

def test_me2():
    assert True

def not_a_test():
    assert True