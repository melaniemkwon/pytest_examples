'''
run tests that match string
pytest -v -s -k "test2 or test3"

run tests that have pytest.mark decorator that match expression
pytest -v -s -m "test1 or test3"
'''
import pytest

@pytest.mark.test3
def test3():
    print("\nTest 3!")
    assert True