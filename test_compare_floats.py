'''
floating point values are internally stored as binary fractions..

pytest 'approx' function can be used to verify two floats
'''
from pytest import approx

# FAIL
# def test_bad_float_compare():
#     assert (0.1 + 0.2) == 0.3

# PASS
def test_good_float_compare():
    assert (0.1 + 0.2) == approx(0.3)