'''
Text Fixture Teardown
Test Fixtures can each have their own optional teardown code
which is called after a fixture goes out of scope.

2 methods for specifying teardown code.
 -yield keyword
 -request-context object's 'addfinalizer' method
'''

import pytest

@pytest.fixture()
def setup1():
    print("\nSetup 1")
    yield
    # code after 'yield' is executed after the fixture goes out of scope
    # yield keyword replaces 'return' keyword
    #   so any return values also specified in yield statement
    print("\nTeardown 1")

@pytest.fixture()
def setup2(request):
    print("\nSetup 2")

    def teardown_a():
        print("\nTeardown A")

    def teardown_b():
        print("\nTeardown B")

    # can use addfinalizer for multiple finalization functions
    request.addfinalizer(teardown_a)
    request.addfinalizer(teardown_b)

def test1(setup1):
    print("Executing test1")
    assert True

def test1(setup2):
    print("Executing test2")
    assert True