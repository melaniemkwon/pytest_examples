'''
test fixtures

-Text Fixtures allow for re-use of setup and teardown code across all tests.
-'pytest.fixture' decorator is applied to functions that are decorators
-Individual unit test can specify which fextures they want executed.
-The autoused parameter can be set to true to automagically execute a
  a fixture before each test.
'''

import pytest

@pytest.fixture(autouse=True)
def setup():
    print("\nSetup")

def test1():
    print("Executing test1")
    assert True

def test2():
    print("Executing test2")
    assert True

# @pytest.fixture()
# def setup():
#     print("\nSetup")

# def test1(setup):
#     print("Executing test1")
#     assert True

# @pytest.mark.usefixtures("setup")
# def test2():
#     print("Executing test2")
#     assert True