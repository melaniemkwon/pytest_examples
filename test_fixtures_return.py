'''
Test Fixtures can optional return data.

We can use optional 'params' array arg in fixture decorator
to specify the data returned to the test.

When a 'params' argument is specified then the test will
be called one time with each value specified.
'''

import pytest

@pytest.fixture(params=[1,2,3])
def setup(request):
    retVal = request.param
    print("\nSetup:  retVal = {}".format(retVal))
    return retVal

def test1(setup):
    print("\nsetup = {}".format(setup))
    assert True